<?php
        if(isset($_POST["contenu"])) {
            file_put_contents("enregistrementVarityper.txt", "\n".$_POST["contenu"], FILE_APPEND);
            header('Location: thankyou.html');
        }
?>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>About</title>
        <link rel="icon" href="faviconVarityper32.png"/>
    </head>
    <style>
        @font-face {
            font-family: OlympicVar;
            src: url(FONT/OlympicGX.ttf);
        }
        
        @font-face {
            font-family: WorkSans;
            src: url(FONT/WorkSans-Regular.ttf);
        }
        
        @font-face {
            font-family: Olympic-BlackRegular;
            src: url(FONT/Olympic-BlackRegular.otf);
        }
        
        @keyframes typeAnimation {
            from {font-variation-settings: 'wght' 20, 'wdth' 50;}
            to {font-variation-settings: 'wght' 100, 'wdth' 50;}
        }

        #title {
            width: 100vw;
            font-family: "OlympicVar";
            font-size: 56px;
            font-kerning: normal;
            margin-left: 48px;
            margin-top: 40px;
            animation-name: typeAnimation;
            animation-duration: 2s;
            animation-direction: alternate; 
            animation-iteration-count: infinite; 
            animation-timing-function: ease-in-out;
            color: #262626;
        }
        
        #text {
            width: 830px;
            font-family: "WorkSans";
            font-size: 28px;
            line-height: 32px;
            margin-left: 50px;
            padding-top: 30px;
            color: #262626;
        }
        
        #back {
            position: fixed;
            bottom: 40px;
            right: 40px;
            font-family: "WorkSans";
            font-size: 28px;
            line-height: 32px;
            margin-left: 50px;
            padding-top: 30px;
            color: #262626;
        }
        
        a {
            color: #262626;
        }
        
        body {
            background-color: #c266ff;
        }
        
        #feedback {
            height: 100px;
            width: 830px;
            background-color: #262626;
            color: #c266ff;
            border: none;
            font-size: 18px;
            font-family: "WorkSans";
            padding: 2px 5px 5px 20px;
        }
        
           
        #feedback:focus {
            outline: none;
        }
        
        
        .button {
            border: none;
            color: #c266ff;
            font-family: "WorkSans";
            font-size: 28px;
            background-color: #262626;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            margin: 20px 0px;
            cursor: pointer;
        }
        
        .button:focus {
            outline: none;
        }
        
        
        
       
     
    </style>
    <body>
        <div id="title">VARITYPER</div>
        <div id="text">Varityper is a simple web-app to set and arrange text using a dedicated variable font.<br><br>You can compose text with ease on a size of canvas you define. Just press cmd+P to export it as a PDF.<br><br>Don’t hesitate to fork it on <a href="https://gitlab.com/Aboscariol/Varityper">gitlab</a>.<br><br>If you want to see more of Olympic, the typeface used in this tool, visit  <a href="http://varityper.eu/olympic.html">this page.</a>
        <br><br>
        <form action="about.php" method="post" autocomplete="off">
            <textarea type="text" name="contenu" id="feedback">As this project is at an early stage, please feel free to give me your feedback by writing a message here. Thank you!</textarea><br>
                <button class="button" name="submit" rows="5" type="submit">Send</button>
        </form></div>

        
        
        <a id="back" href="index.html">Back</a>
    </body>
</html>
